// console.log('eyow powz')

/*
	fetch() is a method in JS, which allows to send request to an API and process its response. fetch has 2 arguments, the url to resource/route and optional object which contains additional information about our requests such as method, the body, and the headers of our request

	fetch() method
	Syntax:
		fetch(url, options)

*/

//Get post data using fetch()
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => console.log(data))

//Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (event) => {

	event.preventDefault();
//Start of fetch
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}

	})
	.then((response) => response.json())
	.then((data) => {
	
	console.log(data)
	alert("Successfully Added.");


	//Clear the text elements upon post creation
	document.querySelector('#txt-title').value = null;
	document.querySelector('#txt-body').value = null


	})
//End of Request
})

//Show Posts
const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Mini Activity: Get or retrieve all the posts.
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data))


//Edit post
const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	//Removes the disabled attribute from the button
	document.querySelector('#btn-submit-update').removeAttribute('disabled')


}

//Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (event) => {

	event.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({

			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	}).then((response) => response.json())
	.then((data)=> {

		console.log(data)
		alert('Successfully updated')

		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null

		document.querySelector('#btn-submit-update').setAttribute('disabled', true)

	})	

})


//Mini-Activity
fetch('https://jsonplaceholder.typicode.com/posts/2').then((response) => response.json()).then((data) => {
		console.log(data)
	})

	
const deletePost = (id) => {
	document.querySelector(`#post-${id}`).remove()
}